package ru.manasyan.se.server.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/")
public class InfoController {

    @GetMapping("info")
    public String info() {
        return (new Date()).toString();
    }
}
