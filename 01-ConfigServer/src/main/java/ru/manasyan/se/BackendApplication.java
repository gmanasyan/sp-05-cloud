package ru.manasyan.se;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;


/**
 *  http://localhost:8888/app-eureka-server.properties
 *  http://localhost:8888/app-server-api.properties
 */
@EnableConfigServer
@SpringBootApplication
public class BackendApplication {
    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }
}
